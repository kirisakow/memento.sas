/* 
	VENTES
	PUB
	D1
	D2
	D3
	LNVENTES
	LNPUB
	t
*/
data pubvente;
set '/home/u43061992/sasuser.v94/PUS_Econometrie-sous-SAS/pubvente.sas7bdat';
run;
/*
Tentons de visualiser la relation entre les ventes et l'investissement en publicité.
*/
proc gplot data=pubvente;
plot ventes*pub;
run;
proc reg data=pubvente;
model ventes=pub / dw;
run;
/*
À première vue, aucune relation linéaire ne peut être constatée. Les deux indicateurs de plausibilité de H_0,
Pr>F et Pr>|t| sont tous deux largement supérieurs à 0.05 => on retient H_0 : le montant des dépenses publicitaires 
n'induit pas d'action significative sur les ventes.

À présent, modélisons et examinons le parcours temporel du résidu calculé.
*/
proc reg data=pubvente;
model ventes=pub / dw r;
run;
/*
On s'aperçoit que les résidus calculés pour les différentes années sont généralement de même signe et 
de même ordre de grandeur pour un trimestre donné. Ceci est très net pour le troisième trimestre pour 
lequel le résidu est toujours négatif. Les ventes semblent être affectées d'un mouvement saisonnier.

On peut confirmer cette impression par un test de Durbin-Watson en examinant si le résidu observé à la date t-1 
a un impact sur le résidu observé à la date t (le modèle : ε_t = ρ⋅ε_{t-1} + η). Testons-le avec 
l'instruction DWPROB
*/
proc reg data=pubvente;
model ventes=pub / dwprob;
run;
/*
On obtient 

	Pr < DW 	0.8708
	Pr > DW 	0.1292
	
	Pr < DW 	0.8962
	Pr > DW 	0.1038

où chacune des valeurs est supérieure à 0.05 	=> 		DW normal	=>		absence d'autocorrélation.

À présent, introduisons dans notre modèle linéaire trois variables muettes (ou dummy) D1, D2 et D3 tenant lieu, 
respectivement, les trois premiers trimestres de l'année. 

Le quatrième semestre a été choisi en tant que modalité de référence du fait que son résidu ne semble pas s'éloigner de 0.
*/
proc reg data=pubvente;
model ventes=pub D1 D2 D3 / dwprob;
run;
/*
Sensiblement plus précis que les précédents (R² = 0.7860) et globalement très 
significatif (Pr > F <.0001), ce nouveau modèle possède les paramètres suivants :

+------------+-----+---------------------+-----------------+----------+----------+
| Variable   | DF  | Parameter Estimate  | Standard Error  | t Value  | Pr > |t| |
+------------+-----+---------------------+-----------------+----------+----------+
| Intercept  |  1  |          111.76260  |       25.16439  |    4.44  | 0.0003   |
| PUB        |  1  |            3.64195  |        0.94466  |    3.86  | 0.0011   |
| D1         |  1  |            9.99347  |       16.32583  |    0.61  | 0.5477   |
| D2         |  1  |           23.72041  |       16.28365  |    1.46  | 0.1615   |
| D3         |  1  |          -86.27479  |       16.28338  |   -5.30  | <.0001   |
+------------+-----+---------------------+-----------------+----------+----------+

	VENTES = 111.76 + 3.64⋅PUB + 9.99⋅D1 + 23.72⋅D2 – 86.27⋅D3

*/

