data import;
/* temps y m */
set '/home/u43061992/sasuser.v94/PUS_Econometrie-sous-SAS/import.sas7bdat';
ln_import=log(m); /* ln des données import */
ln_pib=log(y); /* ln des données PIB */
run;

proc print data=import;
run;

proc reg data=import;
model ln_import=ln_pib;
run;
/*
le modèle obtenu présente un R² = 0.9882 => le modèle explique 98,82% des variations observées de ln_import
                          un t_bilatéral_calculé = 43.88
                          l'équation log-linéaire est : ln_import = 1.51308*ln_pib - 2.37952
                                              et donc :         m = y**(1.51308) * exp(-2.37952)
*/


/* Test de significativité du modèle par le test de Student :
Calcul et affichage pour 
  alpha = 5%
  intervalle de confiance = 100% - 0.5*alpha = 97.5% 
  n-k-1 = 23 degrés de liberté (n = 25, k = 1)
*/
data student;
t_alpha=tinv(.975, 23);
run;

proc print data=student;
run;
/*
on obtient : t_alpha = 2.06866
Règle de décision : DH_0 (Y, le PIB, n'agit pas sur M, les importations) si t_bilatéral_calculé <= t_alpha
                    DH_1 (Y agit sur M significativement, très significativement, hautement significativement) sinon.
or t_bilatéral_calculé = |â|/s_â = 43.88 => on retient DH_1 : d'après le modèle, le PIB agit significativement sur les importations.
*/


/* Test de significativité du modèle par le test de Fisher :
Calcul et affichage pour 
  alpha = 5%
  intervalle de confiance = 100% - alpha = 95% 
  n-k-1 = 23 degrés de liberté (n = 25, k = 1)
*/
data fisher;
f_alpha=finv(.95, 1, 23);
run;

proc print data=fisher;
run;
/*
on obtient : f_alpha = 4.27934
Règle de décision : DH_0 (Y, le PIB, n'agit pas sur M, les importations) si f_calculé <= f_alpha
                    DH_1 (Y agit sur M significativement, très significativement, hautement significativement) sinon.
or f_calculé = 1925.16 => on retient DH_1 : d'après le modèle, le PIB agit significativement sur les importations.
*/



/* Test de significativité du modèle par l'évaluation du coefficient de détermination par le test de Fisher :
Règle de décision : DH_0 <=> R²_calculé <= R²_alpha
                    DH_1 sinon.
Or R²_calculé = 0.9882
et R²_alpha = f_alpha / (f_alpha + (n-k-1) / k) = 4.27934 / (4.27934 + 23/1) = 0.1568
=> On retient donc l'hypothèse H_1 : le modèle est significatif.
*/



/*
Test de l'hypothèse que l'élasticité des importations est supérieure à l'unité
Règle de décision : 
* DH_0 (l'accroissement du PIB provoque un accroissement proportionnel des M) <=> t_unilatéral_droit_calculé <= t_alpha
* DH_1 (l'accroissement du PIB provoque un accroissement plus que proportionnel des M) sinon.
Or t_unilatéral_droit_calculé = (â-1)/s_â = (1.51308-1)/0.03448 = 14.88
et t_alpha = 1.714, avec alpha = 5% et n-k-1 = 23
=> l'élasticité des importations est significativement supérieure à l'unité : 
  l'accroissement du PIB provoque donc un accroissement plus que proportionnel des importations
*/