data oeufs; 
/* largeur hauteur poids */
set '/home/u43061992/sasuser.v94/PUS_Econometrie-sous-SAS/oeufs.sas7bdat';
run;

proc print data=oeufs;
run;

proc gplot data=oeufs;
plot poids*hauteur;
run;

proc reg data=oeufs;
model poids=hauteur;
plot poids*hauteur;
run;

data lnoeufs;
set oeufs;
lnpoids=log(poids);
lnhaut=log(hauteur);
lnlarg=log(largeur);
run;

proc gplot data=lnoeufs;
plot lnpoids*lnhaut;
run;

proc reg data=lnoeufs;
model lnpoids=lnhaut;
run;

data oeufs1;
set oeufs;
/* calculer les valeurs du poids selon le modèle log-linéaire proposé */
a=3.00357;
b=exp(-1.67232);
poids_modelise=b*hauteur**a;
/* calculer l'écart entre le poids observé (réel) et le poids issu de la modélisation */
ecart_poids=poids-poids_modelise;
/* élever ecart_poids au carré */
ecart_poids2 = ecart_poids**2;
run;

proc means sum data=oeufs1;
/* calculer la somme des carrés de ecart_poids */
var ecart_poids2; 
run; 
/* 
la somme des carrés de ecart_poids = 100.3557155 
permet de calculer le pseudo-R² = 1 - (somme ecart_poids2)/NVT
                                = 1 - 100.3557155/7780.90142
                                = 0.9871
*/
