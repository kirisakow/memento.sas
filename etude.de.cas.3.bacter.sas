/*
=== MODÈLE LINÉAIRE ===
*/
data bacter;
/* T (temps) Y (population de bactéries) */
set '/home/u43061992/sasuser.v94/PUS_Econometrie-sous-SAS/bacter.sas7bdat';
run;

proc reg data=bacter;
model Y=T;
run;
/*
modèle obtenu : Y = 2.38182 * T - 1.27273
avec R² = 0.8883 => le modèle linéaire explique 88,8% des variations observées
*/

/*
=== MODÈLE EXPONENTIEL semi-logarithmique ===
*/
data lnbacter;
set bacter;
lnY=log(Y);
run;

proc reg data=lnbacter;
model lnY=T;
run;
/*
modèle obtenu : lnY = 0.25223 * T + 0.80447 <=> Y = exp(0.25223 * T + 0.80447)
avec R² = 0.9963 
Toutefois, ce R² semi-log n'est pas comparable avec le R² linéaire 
=> ramener R² semi-log à un pseudo-R² comparable avec le R² du modèle linéaire */
/*
=== PSEUDO R² ===
*/
data pseudoR;
set bacter;
Ymodelise=exp(0.25223 * T + 0.80447);
ecart_modelise=(Y-Ymodelise);
ecart_modelise2=ecart_modelise**2;
run;

proc means sum data=pseudoR;
var ecart_modelise2;
run;
/* 
la somme des ecart_modelise² = 1.0902122
permet de calculer le pseudo-R² = 1 - (somme des ecart_modelise²)/NVT
                                = 1 - 1.0902122/702.54545
                                = 0.99844819691 => le modèle exponentiel explique plus de 99,8% des variations observées
*/

/*
=== TEST DE SIGNIFICATIVITÉ DU MODÈLE EXPONENTIEL ===

H_0 : T n'agit pas sur Y <=> a = 0
H_1 : T agit sur Y       <=> a != 0

F Value = 71.54
t Value = 8.46

Les deux indicateurs de plausibilité de l'hypothèse H_0, Pr > F et Pr > |t|, sont tous deux < 0.0001. 
On retient donc l'hypothèse H_1 : l'action du temps sur le nombre de bactéries est hautement significative.
*/

/*
=== PRONOSTIC POUR LA 25e HEURE ===

Modèle retenu : Y(T) = exp(0.25223 * T + 0.80447)
Pronostic pour Y(25) = exp(0.25223 * 25 + 0.80447) = 1224, avec l'intervalle de confiance de (à calculer) :
*/

proc means mean var data=lnbacter;
run;

data interval;
n=11; 				/* nombre d'observations */
rootMSE=0.05391;	/* valeur "Root MSE" du modèle semi-log */
s_e2=rootMSE**2; 	/* variance des résidus : "Root MSE" élevée au carré */
T_p=25; 			/* date pour laquelle on veut la prévision */
s2_T=11;			/* commande `proc means mean var data=lnbacter` -> récupérer valeur "Variance"
						mais en vrai c'est pas une variance : c'est l'estimateur sans biais de T */
T_moy=5;			/* commande `proc means mean var data=lnbacter` -> récupérer valeur "Mean" */
s_yy_p2=s_e2*(1+1/n+(T_p-T_moy)**2/((n-1)*s2_T)); /* variance y-y_p */
t_calcule=tinv(0.975,(11-1-1));
lnY_modelise=0.25223*25+0.80447; /* lnY modelise pour T=25 */
lnY_borneInf=lnY_modelise-t_calcule*s_yy_p2**0.5;
lnY_borneSup=lnY_modelise+t_calcule*s_yy_p2**0.5;
Y_borneInf=exp(lnY_borneInf);
Y_borneSup=exp(lnY_borneSup);

proc print data=interval;
var Y_borneInf Y_borneSup; /* à défaut de préciser les variables qu'on veut, SAS affichera toutes les var de ce data */
run;

/*
Valeurs obtenues :
	Y_borneInf = 939.236
	Y_borneSup = 1596.19
*/

/*
Une autre manière d'estimer la population à T=25 : 
*/
data bacter2;
input T Y;
cards;
25 .
;
run;

data lnbacter2;
set bacter bacter2;
lnY=log(Y);
run;

proc reg data=lnbacter2;
model lnY=T /cli; /* L'option CLI indique à SAS de calculer une estimation pour lnY là où les valeurs Y sont absentes */
run;
/*
On peut lire le résultat estimé : lnY(T=25)=7.1103 
                             => exp(7.1103)=1224.5148
ainsi que l'intervalle de confiance à 95% de lnY : [6.8452 ; 7.3755] 
                                  => [exp(6.8452)=939.3611 ; exp(7.3755)=1596.3898]
*/






