Aide-mémoire de programmation SAS

# TD 1

## **Exercice 1 :**

```
LIBNAME td1 "C:\TDSAS";
TITLE "TITRE";
FOOTNOTE "NOTE DE BAS DE PAGE";

DATA exemple;
SET td1.siad;
RUN;

PROC PRINT DATA=exemple;
RUN;
```

## **Exercice 2 :**

- ### Créer dans votre répertoire personnel la table « numerique » comprenant les variables décrites ci-dessous et en saisissant les données suivantes :

```
LIBNAME exo2 "C:\TDSAS";

DATA exo2.numerique;
INPUT X1 X2 X3 X4 X5;
CARDS;
10 15 14 21 20.6
51 14.1 32.5 40 15
84 74 106 10.4 19.7
45 35 86.4 70.25 98.13
34 78 95 68 28
151.2 14.8 17.7 15.9 106.5
151.2 14.8 17.7 15.9 106.5
151.2 14.8 17.7 15.9 106.5
;
RUN;
```

- ### Créer ensuite dans votre répertoire personnel la table « alphanumérique » comprenant les variables décrites ci-dessous et en saisissant les données suivantes : 

```
DATA exo2.alphanumerique;
INPUT A1 $ A2 $ A3 $ A4 $;
CARDS;
CLAIRE FEMME LYON CDI
THOMAS HOMME PARIS CDD
SABINE FEMME LILLE STAGE
SYLVAIN HOMME BORDEAUX CDI
BENOIT HOMME NANTES CDI
MARTINE FEMME PARIS CDD
LAURA FEMME LILLE STAGE
;
RUN;
```
- ### Créer ensuite dans votre répertoire personnel la table « mixte » comprenant les variables décrites ci-dessous et en saisissant les données suivantes :

```
DATA exo2.mixte;
INPUT Prenom $ Age Anglais Gestion SGBD Econometrie SAS Sexe $;
CARDS;
OLIVIER . 12 14.5 14 13.5 14 H
LUCIE 20 8.5 7 10 . 10.5 F
THEO 24 11 11 14.5 10.5 7 .
KARINE 21 10.5 10 12.5 9.5 14 F
ANAIS 22 . 11.5 . 10 14 F
. 20 11 9.5 12 10.5 9 F
PIERRE 23 7 12.5 13.5 14.5 13 H
;
RUN;
```

## **Exercice 3 :**

* ### Programme 1 :

```
DATA pgm1;
INPUT Prenom $ Sexe $ Age Moyenne;
CARDS;
THOMAS
BEATRICE . 18 13.2
CLAIRE F 20 8.6
PIERRE H . 15.4
;
RUN;
```

* ### Programme 2 :

```
DATA pgm2;
CARDS;
THOMAS H 18  11
BEATRICE F 14 10
 F 7 10 11
PIERRE H 19 5 17
;
RUN;
```

* ### Programme 3 :

```
DATA pgm3;
INPUT Prenom $ Sexe $ Age Moyenne;
CARDS;
THOMAS 1 18 15.4
BEATRICE F 18 13.2
CLAIRE F 20 8.6
PIERRE H
;
RUN;
```

* ### Programme 4 :

```
DATA pgm4;
INFILE CARDS MISSOVER;
INPUT Prenom $ Sexe $ Age Moyenne;
CARDS;
THOMAS H 18  11
BEATRICE F 14 10
 F 7 10 11
PIERRE H 19 5 17
;
RUN;
```

* ### Programme 5 :

```
DATA pgm5;
INPUT Prenom $ Sexe $ Age Moyenne;
CARDS;
THOMAS
BEATRICE 2 18 13.2
CLAIRE F .
. H . 15.4 12 22
;
RUN;
```

















# TD 2

## **Exercice 1**
- ### Visualisez le `fichier1.csv` et repérez les différents champs à importer
- ### Créer une table SAS à partir de ce fichier externe
- ### Vérifier que le contenu de votre table est bien complet.

```
FILENAME fic 'C:\Documents and Settings\';

DATA fichier1;
LENGTH prenom ville $40; /*si l'on ne spécifie pas la taille, elle sera coupé a 8 */
INFILE fic("fichier1.csv") DLM=';' FIRSTOBS=2 DSD; /* DSD permet de ne pas sauter les valeurs manquantes */
INPUT prenom $ ville $ civ $ cp $;
RUN;
```

## **Exercice 2**

- ### Visualisez le `fichier2.txt` et repérez les différents champs à importer
- ### Créer deux tables SAS à partir de ce fichier externe. L’une à l’aide de format et l’autre en la lisant en mode colonne.
- ### Lorsque vous lirez la table en mode colonne, créer une nouvelle variable avec l’initiale du prénom
- ### Vérifier que le contenu de votre table est bien complet.

```
FILENAME fic 'C:\Documents and Settings\';

DATA fichier2;
INFILE fic("fichier2.txt"); 
INPUT civ $3. prenom $40. cp $5. ville $40.;
RUN;
```
### Ou en mode colonne :
```
DATA fichier2bis;
INFILE fic("fichier2.txt"); 
INPUT civ $1-3 prenom $4-43 @44 cp $5. ville $49-88;
RUN;
```

## **Exercice 3**

### **Question 1 :** Créer une table SAS à partir du fichier externe nommé `fichier3`. Vérifier que le contenu de votre table est bien complet.
```
FILENAME fic 'C:\Documents and Settings\';

DATA fichier3;
INFILE fic("fichier3");
INPUT numcli 3. numcompte 5. datouvcompte YYMMDD10.;
run;
```
### **Question 2 :** Exporter votre table dans le format que vous souhaitez
```
DATA _NULL_;
SET fichier3;
FILE 'C:\Documents and Settings\export.txt';
PUT numcli numcompte datouvcompte;
RUN;
```

## **Exercice 4**

### La réponse n'est pas évidente :
- La variable X1 n'est pas forcément caractère puisqu'il est possible de déterminer la modalité A comme symbole de valeur manquante
- La variable X2 est a priori numérique mais vous pouvez donner à une variable caractère ces modalités
- La variable X3 est normalement caractère car nous observons que des blancs. Cependant, à l'aide d'un FORMAT, vous pouvez faire apparaître ce type de modalités pour une variable numérique
- La variable X4 est a priori numérique mais il est aussi possible d'insérer des nombres dans une variable caractère
- La variable X5 est a priori caractère mais avec des valeurs manquantes ou des FORMAT, il est possible d'avoir derrière ces lettres une variable numérique
- La variable X6 est a priori caractère (à cause du blanc en valeur manquante mais comme vous l'aurez deviné, il est aussi possible qu'elle soit numérique...;

## **Exercice 5**

- ### A partir de la table SIAD, créez un format que nous appellerons mention. Ce format nous permettra de déterminer la mention obtenue par l’étudiant suivant sa moyenne.

```
LIBNAME td '\\Srvfsesetu\partage enseignants\Matthieu Lehobey\TD2';

PROC FORMAT ;
VALUE mention
LOW -< 10 = "Rattrapage"
10 -< 12 = "Passable"
12 -< 14 = "Assez Bien"
14 -< 16 = "Bien"
16 -< 18 = "Très Bien"
18 - HIGH = "Excellent"
;
RUN;

DATA siad2;
SET td.siad;
FORMAT moyenne mention.;
RUN;

PROC PRINT;
FORMAT moyenne mention.;
RUN;
```
- ### A l’aide d’une étape DATA, exporter la table SIAD au format CSV avec comme séparateur le point-virgule. Vous exporterez seulement les variables « prénom » et « moyenne ».

```
DATA _NULL_;
SET siad2;
FILE 'C:\Documents and Settings\export.CSV' DLM=";";
PUT prenom moyenne;
RUN;
```

## **Exercice 6**

### Importez, à l’aide d’une étape DATA, le fichier `acteur.txt`

```
DATA exo6;
INFILE "C:\Documents and Settings\acteur.txt" FIRSTOBS=2; 
INPUT prenom $1-16 nom $17-30 film $31-53 datnai 4.;
RUN;
```
















# TD 3

## **Exercice 1**
### 1.	Créez la table « moyenne » dans laquelle vous calculerez la moyenne pour chaque élève (sans utiliser de fonction). 
```
libname td3 'C:\Users\149846\Documents\My SAS Files\9.4\TD3';

DATA moyenne;
SET td3.siad;
moyenne = (anglais+gestion+sgbd+econometrie+sas) / 5;
RUN;

PROC PRINT DATA=moyenne;
RUN;
```

### 2.	A partir de la table « moyenne », créez une nouvelle variable « admission » qui vaudra « Oui » si la note est supérieure ou égale à 10 et « Non » sinon.

```
DATA moyenne;
SET moyenne;
IF moyenne >= 10 THEN admission = "Oui";
ELSE admission = "Non";
RUN;
```
### 3.	A partir de la table « moyenne », conservez uniquement les élèves ayant une moyenne strictement supérieure à 12.5 et inférieure ou égale à 15. Vous ne conserverez que les variables « prénom » et « moyenne ».
```
DATA moyenne;
SET moyenne;
IF 12.5 < moyenne <= 15 THEN OUTPUT;
KEEP prenom moyenne;
RUN;

PROC PRINT DATA=moyenne;
RUN;
```

### 4.	A l’aide d’une seule étape DATA, créez les tables suivantes :
### -	une table avec les élèves ayant au moins une note strictement inférieure à 9,
### -	une table avec les élèves ayant toutes leurs notes strictement supérieures à 14,
### -	une table avec les élèves ayant une note inférieure ou égale à 10 en Gestion, âgé de moins de 20 ans et de sexe féminin

```
libname td3 'C:\Users\149846\Documents\My SAS Files\9.4\TD3';

DATA inf9 sup14 gestion;
SET td3.siad;
IF anglais < 9 OR gestion < 9 OR sgbd < 9 OR econometrie < 9 OR sas < 9 THEN OUTPUT inf9;
IF anglais > 14 AND gestion > 14 AND sgbd > 14 AND econometrie > 14 AND sas > 14 THEN OUTPUT sup14;
IF gestion <= 10 AND age < 20 AND sexe = "F" THEN OUTPUT gestion;
RUN;

PROC PRINT DATA=inf9;
RUN;

PROC PRINT DATA=sup14;
RUN;

PROC PRINT DATA=gestion;
RUN;
```

## **Exercice 2**

### A partir de la table SIAD :
### -	calculer à nouveau la moyenne par étudiant à l’aide d’une fonction SAS,
### -	calculer la dispersion des notes par étudiant (écart-type, minimum et maximum),
### -	conserver que le prénom et les nouvelles variables calculées.

```
libname td3 'C:\Users\149846\Documents\My SAS Files\9.4\TD3';

DATA fonctions_num;
SET td3.siad;
moyenne = mean(anglais,gestion,sgbd,econometrie,sas);
minimum = min(anglais,gestion,sgbd,econometrie,sas);
maximum = max(anglais,gestion,sgbd,econometrie,sas);
ecart_type = std(anglais,gestion,sgbd,econometrie,sas);
KEEP prenom moyenne minimum maximum ecart_type;
RUN;

PROC PRINT DATA=fonctions_num; 
RUN;
```

## **Exercice 3**

### A partir de la table SIAD, créer différentes variables pour chaque étudiant :
-	### la variable « majmin » permettant de mettre la 1ère lettre du prénom en majuscule et le reste en minuscule
-	### la variable « longueur » donnant la longueur de son prénom
-	### la variable « minuscule » donnant le prénom en minuscule
-	### la variable « chaine » qui sera la concaténation des éléments suivants :
    - ### élément 1 : initiale du prénom
    - ### élément 2 : note maximum de l’élève
    - ### élément 3 : l’âge de l’élève
    - ### séparateur : chacun des éléments étant séparé par le symbole « / »

```
DATA fonctions_car;
SET td3.siad;
majmin = PROPCASE(prenom);
longueur = LENGTH(prenom);
minuscule = LOWCASE(prenom);
chaine = CATX("/", SUBSTR(prenom,1,1), MAX(anglais,gestion,sgbd,econometrie,sas), AGE);
RUN;

PROC PRINT DATA=fonctions_car; 
RUN;
```

## **Exercice 4**

### Depuis la table SIAD, extraire les étudiants dont le prénom :
### -	commence par « al »,
### -	contient « ma »,
### -	se termine par « ie »,
### -	à pour 2ème lettre un « a » et pour 4ème lettre un « i ».

```
DATA td3.filtre;
SET td3.siad;
WHERE prenom LIKE 'AL%';
RUN;
```

```
DATA td3.filtre;
SET td3.siad;
WHERE prenom CONTAINS 'MA';
RUN;
```

```
DATA td3.filtre;
SET td3.siad;
WHERE prenom LIKE '%IE';
RUN;
```

```
DATA td3.filtre;
SET td3.siad;
WHERE prenom LIKE '_A_I%';
RUN;
```

## **Exercice 5**

### Créez quatre variables permettant de calculer votre âge en : années, mois, jours, minutes.
### A quelle date aurez-vous 50 ans ?

```
DATA age;
date_naiss = '25JAN1996'd;
date_naiss_dt = '25JAN1996:21:15:34'dt;

annee=INTCK('YEAR', date_naiss, DATE());
annee2=(DATE()-date_naiss)/365;

mois=INTCK('MONTH', date_naiss, DATE());
mois2=(DATE()-date_naiss)/365*12;

jours=INTCK('DAY', date_naiss, DATE());
jours2=DATE()-date_naiss;

minutes=INTCK('MINUTE', date_naiss_dt, DATETIME());
minutes2=(DATETIME()-date_naiss_dt)/60;

cinquante=INTCK('YEAR', date_naiss, 50);

RUN;

PROC PRINT DATA=age;
RUN;
```

















# TD 4

## **Exercice 1 :**

### 1.	A l’aide d’une boucle DO, créez une table SAS nommée « multiple » contenant dans une variable X1 tous les multiples de 5 compris entre 15 et 50 inclus et dans une variable X2 la racine carrée de tous ces multiples.

```
DATA multiple;
DO i=15 TO 50 BY 5;
	X1=i;
	X2=ROUND(SQRT(i));
	OUTPUT;
END;
KEEP i X1 X2;
RUN;
```

### 2.	En reprenant la variable X1 précédemment créée, affichez sur une seule ligne la liste des multiples de 5 compris entre 15 et 50.
```
DATA liste_multiple;
SET multiple end=fin_liste;
LENGTH liste $65;
RETAIN liste 'Multiples de 5 entre 15 et 50 : ';
liste=CATX(" ",liste,x1);
IF fin_liste THEN OUTPUT;
KEEP liste;
RUN;
```
### Autre possibilité :
```
DATA ligne;
SET multiple end=fin;
LENGTH ligne $ 100;
RETAIN ligne 'Valeurs de X1 : ';
ligne=CATX(" ", ligne, X1);
IF fin;
KEEP ligne;
RUN;

PROC PRINT DATA=ligne;
RUN;
```

## **Exercice 2 :**

### Vous utiliserez pour cet exercice la table SAS « note » :

### 1.	A partir de la table « note », et à l’aide d’une étape DATA, supprimez tous les prénoms en doublons. Vous ne conserverez que la variable « prénom ».

```
PROC SORT DATA=note;
BY prenom;
RUN; 


DATA note1;
SET note;
BY prenom;
IF first.prenom THEN OUTPUT;
KEEP prenom;
RUN;
```
### Autre possibilité :
```
libname td4 'C:\Users\149846\Documents\My SAS Files\9.4\TD4';

PROC SORT data=td4.note;
BY prenom ;
RUN ;

DATA td4.note2;
SET td4.note;
IF Prenom=LAG(Prenom) THEN DELETE;
KEEP Prenom;
RUN;
```
### Autre possibilité :
```
libname td4 'C:\Users\149846\Documents\My SAS Files\9.4\TD4';

DATA td4.note2;
SET td4.note;
BY Prenom;
IF first.Prenom THEN OUTPUT;
KEEP Prenom;
RUN;
```

### 2.	A partir de la table « note », et à l’aide de la procédure adéquate, supprimez tous les prénoms en doublons. Vous ne conserverez que la variable « prénom ».

```
PROC SORT DATA=note OUT=note2 (KEEP=prenom) NODUPKEY;
BY prenom;
RUN;
```

### 3.	A partir de la table, calculez le nombre de notes et la somme des notes obtenues par chaque étudiant au cours des 2 années de formations (2010 et 2011). Calculez ensuite la moyenne pour chacun des étudiants.

```
PROC SORT DATA=note;
BY prenom annee;
RUN; 

DATA note3 (KEEP=prenom nb_note somme_note moy);
SET note;
RETAIN nb_note somme_note;
BY prenom annee;
IF first.prenom THEN DO;
	nb_note=1;
	somme_note=note;
END;
ELSE DO;
	nb_note=nb_note+1;
	somme_note=somme_note+note;
END;
IF last.prenom THEN DO;
	moy=somme_note/nb_note;
	OUTPUT;
END;
RUN;
```

### 4.	Indiquez le nombre de notes obtenues par chaque étudiant pour chaque année et calculez une moyenne par année pour chaque étudiant.

```
PROC SORT DATA=note;
BY prenom annee;
RUN; 

DATA note4 (KEEP=prenom annee nb_note somme_note moy);
SET note;
RETAIN nb_note somme_note;
BY prenom annee;
IF first.annee THEN DO ;
	nb_note=1;
	somme_note=note;
END;
ELSE DO ;
	nb_note=nb_note+1;
	somme_note=somme_note+note;
END;
IF last.annee THEN DO ;
	moy=somme_note/nb_note;
	OUTPUT;
END;
RUN;
```

### 5.	Calculez une moyenne générale tous étudiants confondus, par année.

```
PROC SORT DATA=note;
BY annee;
RUN; 

DATA note5 (KEEP=annee nb_note somme_note moy);
SET note;
RETAIN nb_note somme_note;
BY annee;
IF first.annee THEN DO ;
	nb_note=1;
	somme_note=note;
END;
ELSE DO;
	nb_note=nb_note+1;
	somme_note=somme_note+note;
END;
IF last.annee THEN DO ;
	moy=somme_note/nb_note;
	OUTPUT;
END;
RUN;
```

### 6.	A l’aide de la procédure adéquate, créez une nouvelle table avec une ligne par étudiant et autant de colonnes que l’étudiant à de notes.

```
PROC SORT DATA = note;
BY prenom;
RUN; 

PROC TRANSPOSE DATA = TD3.note  OUT = note_transpose PREFIX = note_;
BY prenom;
VAR note;
RUN;
```

## **Exercice 3 :**

### A partir de la table enquete2, à l’aide d’une boucle ARRAY notamment, créer une table à 1 observation et 8 variables contenant :
 
### -	le nombre d’enquêtés ayant choisi les modalités 1 à 8 pour la variable Q1

```
DATA tableau;
SET td4.enquete END=fin;
ARRAY T(8) T1-T8;	/*on déclare les 8 variables de comptage*/
DO i=1 TO 8;
	j=i;
	T(i)+(Q1=j);
END;
IF fin THEN OUTPUT;
KEEP T1-T8;
RUN;
```

### -	adaptez ce programme afin d’obtenir le pourcentage d’enquêtés ayant choisi les modalités 1 à 8 pour la variable Q1.

```
DATA tableau2;
SET td4.enquete END=fin;
tot+1;
ARRAY T(8) T1-T8;	/*on déclare les 8 variables de comptage*/
ARRAY P(8) P1-P8;	/*on déclare les 8 variables de pourcentage*/
DO i=1 TO 8;
	j=i;
	T(i)+(Q1=j);
END;
IF fin THEN DO;
	DO i=1 TO 8;
		P(i)=T(i)/tot;
	END;
	KEEP P1-P8;	/*on conserve les 8 variables de pourcentage*/
	OUTPUT;
END;
RUN;
```

















# TD 5

## **Exercice 1 :**

### 1. Créer deux tables suivantes, ANCIEN et MAJ.
```
libname td5 'C:\Users\149846\Documents\My SAS Files\9.4\TD5';

DATA ancien;
INPUT id salaire;
CARDS;
1 1000
2 1250
3 980
;
RUN;

DATA maj;
INPUT id salaire;
CARDS;
1 1250
2 1180
2 1200
;
RUN;
```
### 2. Concaténer les tables ANCIEN et MAJ.

```
DATA question2;
SET ancien maj;
RUN;
```

### 3. Trier la table obtenue en 2- suivant la variable ID. Ne conserver que la 1ère occurrence de chaque modalité de la variable ID.

```
PROC SORT DATA=question2 OUT=question3 NODUPKEY;
BY id;
RUN;
```
### Autre méthode :

```
PROC SORT DATA=question2 OUT=question3bis;
BY id;
RUN;

DATA question3bis;
SET question3bis;
BY id;
IF FIRST.id THEN OUTPUT;
RUN;
```

### 4. Trier la table obtenue en 2- suivant la variable ID. Ne conserver que la dernière occurrence de chaque modalité de la variable ID.

```
PROC SORT DATA=question2 OUT=question4;
BY id;
RUN;

DATA question4;
SET question4;
BY id;
IF LAST.id THEN OUTPUT;
RUN;
```

### 5. Trier la table obtenue en 2- suivant la variable ID. Ne conserver que l’occurrence de chaque modalité de la variable ID pour laquelle le salaire est le plus élevé.

```
PROC SORT DATA=question2 OUT=question5;
BY id salaire;
RUN;

DATA question5;
SET question5;
BY id;
IF LAST.id THEN OUTPUT;
RUN;
```
### Autre méthode :
```
DATA td5.fusion5;
SET td5.fusion;
RETAIN salaire_max;
BY id;
IF first.id THEN salaire_max=salaire;
ELSE IF salaire > salaire_max THEN salaire_max=salaire;
IF last.id THEN OUTPUT;
KEEP id salaire_max;
RUN;
```

### 6. Fusionner les tables ANCIEN et MAJ sous le contrôle de la variable ID. Conserver les deux variables SALAIRE de ces deux tables.

```
PROC SORT DATA=ancien;
BY id;
RUN;

PROC SORT DATA=maj;
BY id;
RUN;

DATA question6;
MERGE ancien(RENAME=(salaire=salaire1)) maj(RENAME=(salaire=salaire2));
BY id;
RUN;
```

### 7. Fusionner les tables ANCIEN et MAJ sous le contrôle de la variable ID. Ne conserver que les modalités de la variable ID présentes dans ANCIEN mais pas dans MAJ.

```
DATA question7;
MERGE ancien(IN=a) maj(IN=b);
BY id;
IF a AND NOT b;
RUN;
```

### 8. Fusionner les tables ANCIEN et MAJ sous le contrôle de la variable ID. Ne conserver que les modalités de la variable ID présentes dans ANCIEN et dans MAJ.

```
DATA question8;
MERGE ancien(IN=a) maj(IN=b);
BY id;
IF a AND b;
RUN;
```

### 9. Mettre à jour ANCIEN par les données contenues dans MAJ. 

```
DATA question9;
UPDATE ancien maj;
BY id;
RUN;
```

## **Exercice 2 :**

### A partir des tables CLASSE et RESUL, créer trois tables en une seule étape DATA :

### -	la première est constituée des élèves ayant été admis et de plus de treize ans. Nous conservons dans cette table le prénom, l’âge, le sexe et les différentes notes de cours.

### -	la seconde est constituée des élèves ayant été refusés et ayant une note de chimie inférieure à 10. Nous ne conservons dans cette table que le prénom.

### -	la troisième est constituée des élèves pour lesquels nous ne disposons pas des variables de notes et d’âge. Nous conservons dans cette table le prénom, le sexe et la variable « réussite » .


```
libname td5 'C:\Users\149846\Documents\My SAS Files\9.4\TD5';

PROC SORT DATA=td5.classe OUT=classe;
BY prenom;
RUN;

PROC SORT DATA=td5.resul OUT=resul;
BY prenom;
RUN;

DATA classe1(DROP=reussite) classe2(KEEP=prenom) classe3(KEEP=prenom sexe reussite);
MERGE classe(IN=a) resul(IN=b);
BY prenom;
IF a AND b AND reussite IN(1,2) AND age>=13 THEN OUTPUT classe1;
IF a AND b AND reussite IN(3) and chimie<10 THEN OUTPUT classe2;
IF b AND NOT a THEN OUTPUT classe3;
RUN;
```

## **Exercice 3 :**

### 1- Créer deux variables pour chaque ménage :
### -	l’une donnant le nombre d’achats souhaités,
### -	l’autre le nombre de magasins dans lesquels le ménage a l’intention de faire ses achats.

```
DATA question1;
SET td5.enquete;
nbachat = SUM(of A1-A5);
nbmag = SUM(of M1-M4);
KEEP ident nbachat nbmag;
RUN;
```

### 2- A l’aide d’un vecteur ARRAY, créez une table contenant deux variables : l’identifiant client et une variable ACHAT. La variable ACHAT vaudra :
### -	1 si le ménage a l’intention d’acheter un réfrigérateur, 
### -	2 si le ménage a l’intention d’acheter une machine à laver, 
### -	3 si le ménage a l’intention d’acheter un séchoir, 
### -	4 si le ménage a l’intention d’acheter un lave-vaisselle,
### -	5 si le ménage a l’intention d’acheter un four micro-ondes.

```
DATA question2;
SET td5.enquete;
ARRAY VA(5) A1-A5;
DO i=1 TO 5;
	IF VA(i)=1 THEN DO;
		achat=i;
		OUTPUT;
	END;
END;
KEEP ident achat;
RUN;
```
















# TD 6

## **Exercice 1 :**

### 1- A partir de la table `client`, créer une table contenant deux nouvelles variables : 
###    - `AGE` : âge du client
###    - `TRANCHAGE` : tranche d’âge du client, cette variable aura pour valeurs :
### 		1 : < 30 ans
### 		2 :  30-50 ans
### 		3 : > 50 ans
### 		Z : lorsque l’âge est à valeur manquante

```
libname td6 'C:\Users\149846\Documents\My SAS Files\9.4\TD6';

DATA question1;
SET td6.client;
age=YEAR(TODAY()) - annais;

IF age=. THEN tranchage='Z';
ELSE IF age<30 THEN tranchage='1';
ELSE IF 30<=age<=50 THEN tranchage='2';
ELSE tranchage='3';
RUN;
```

### 2- A partir de la table `client`, construire une table ne contenant que les femmes célibataires et de plus de 40 ans. Ne conserver que l’identifiant `client`.

```
DATA question2;
SET td6.client;
WHERE sexe='1' AND sitfam='1' AND annais>1975;
KEEP id;
RUN;
```

### 3- Afficher dans la Sortie la table `client` avec :
### -	les variables identifiant, sexe (avec le format `$fsexe` décrit ci-dessous) et situation familiale (avec le format `$fsitfam` décrit ci-dessous)
### -	un libellé pour les variables conservées
### -	un titre pour cette sortie

```
libname td6 'C:\Users\149846\Documents\My SAS Files\9.4\TD6';

PROC FORMAT;
VALUE $fsexe 1="FEMININ"
             2="MASCULIN";
VALUE $fsitfam 1="CELIBATAIRES"
               2="MARIES"
               3="CONCUBINS"
               4="VEUFS"
               5="DIVORCES";
RUN;

PROC PRINT DATA=td6.client LABEL NOOBS;
VAR id sexe sitfam;
LABEL id="Identifiant" sexe="Sexe du Client" sitfam="Situation Familiale";
FORMAT sexe $fsexe. sitfam $fsitfam.;
TITLE "Table des clients";
RUN;
```

### 4- Fusionner les tables `CLIENT` et `ACHAT` et ne garder que les enregistrements communs à ces deux tables. Isoler dans une autre table les clients non présents dans la table `ACHAT`.

```
PROC SORT DATA=td6.client OUT=client;
BY id;
RUN;
                                                                                                                                        
PROC SORT DATA=td6.achat OUT=achat;
BY id;
RUN;
                                                                                                                                        
DATA commun different;
MERGE client(IN=A) achat(IN=B);
BY id;
IF a AND b THEN OUTPUT commun;
IF a AND NOT b THEN OUTPUT different;
RUN;
```

### 5- A partir de la table créée dans la question 4-, créez une table que nous nommerons `CLICUM` et qui aura les caractéristiques suivantes :
    - ne garder qu’un enregistrement par client, 
    - cumuler pour chaque client :
        * le nombre et le montant d’achats de type A 
        * le nombre et le montant d’achats de type B 
        * le nombre et le montant d’achats de type C 
        * le nombre et le montant d’achats de type D 
    - ne conserver que l’identifiant client et les variables nouvellement créées.
```
PROC SORT DATA=commun;
BY id type;
RUN;

DATA clicum;                                                                                                                              
SET commun;
BY id type;
If First.ID then do ; 
 if Type = "A" then do  ; 
	CA=1 ;
	CB=0 ;
	CC=0 ;
	CD=0 ;
	MA = Montant  ; 
	MB = 0  ; 
	MC = 0  ; 
	MD = 0  ; 
 end; 
 if Type = "B" then do  ; 
	CA=0 ;
	CB=1 ;
	CC=0 ;
	CD=0 ;
	MA = 0  ; 
	MB = Montant  ; 
	MC = 0  ; 
	MD = 0  ; 
 end; 
 if Type = "C" then do  ; 
	CA=0 ;
	CB=0 ;
	CC=1 ;
	CD=0 ;
	MA = 0  ; 
	MB = 0  ; 
	MC = Montant  ; 
	MD = 0  ; 
 end; 
 if Type = "D" then do  ; 
	CA=0 ;
	CB=0 ;
	CC=0 ;
	CD=1 ;
	MA = 0  ; 
	MB = 0  ; 
	MC = 0  ; 
	MD = Montant  ; 
 end; 
end ; 
else do ; 
 if Type = "A" then do  ; 
	CA + 1 ;
	MA + Montant  ; 
 end; 
 if Type = "B" then do ; 
CB + 1 ; 
MB + Montant  ;
 end;
 if Type = "C" then do ; 
CC + 1 ;
MC + Montant  ; 
 end;
 if Type = "D" then do ; 
CD + 1 ; 
MD + Montant  ;
 end;
end ; 
*if last.id then output;
*KEEP ID CA CB CC CD MA MB MC MD ; 
run ;
```

## **Exercice 2 :**

### A partir de la table `SIAD`, et de différentes `PROC PRINT`, éditez les sorties suivantes :

```
PROC FORMAT;
VALUE $fsexe 
	"F"="FEMININ" 
	"M"="MASCULIN";
RUN;
```

### 1) Affichez l’ensemble des observations et des variables :
```
TITLE "PROC PRINT - QUESTION 1";

PROC PRINT DATA=td6.siad;
RUN;
```

### 2) Affichez l’ensemble des observations et des variables en séparant les résultats selon le sexe :
```
TITLE "PROC PRINT - QUESTION 2";

PROC PRINT DATA=td6.siad SPLIT="/";
BY sexe;
LABEL prenom="PRENOM DE/ L'ELEVE"
age="AGE DE/ L'ELEVE";
FORMAT sexe $fsexe.;
RUN;
```

### 3) Affichez l’ensemble des observations et les variables sexe, prénom et gestion en regroupant les résultats selon le sexe et en calculant la somme des notes de gestion :
```
TITLE "PROC PRINT - QUESTION 3";

PROC PRINT DATA=td6.siad SPLIT="/";
VAR prenom sexe gestion;
ID sexe;
SUM gestion;
LABEL prenom="PRENOM DE/ L'ELEVE";
FORMAT sexe $fsexe.;
RUN;
```
### 4) Affichez l’ensemble des observations et les variables sexe, prénom, âge et gestion en séparant les résultats selon le sexe et en calculant la somme des notes de gestion par sexe et au cumulé :
```
TITLE "PROC PRINT - QUESTION 4";

PROC PRINT DATA=td6.siad SPLIT="/";
VAR prenom sexe age gestion;
BY sexe;
ID age;
SUM gestion;
LABEL prenom="PRENOM DE/ L'ELEVE";
FORMAT sexe $fsexe.;
RUN;
```
















# TD 7

## **Exercice 1 :**

### 1) A partir de la table `siad`, et de la `PROC MEANS`, calculez la note moyenne, minimum et maximum ainsi que le nombre de notes pour les matières suivantes : Anglais, Gestion, SGBD, Econométrie et SAS.

```
LIBNAME td7 'C:\Users\149846\Documents\My SAS Files\9.4\TD7';

PROC MEANS DATA=td7.siad N MIN MAX MEAN;
VAR anglais gestion sgbd econometrie sas;
RUN;
```

### 2) A partir de la table `siad`, et de la `PROC MEANS`, présentez les mêmes résultats par sexe puis par age.

```
PROC MEANS DATA=td7.siad N MIN MAX MEAN;
CLASS sexe;
VAR anglais gestion sgbd econometrie sas;
RUN;

PROC MEANS DATA=td7.siad N MIN MAX MEAN;
CLASS age;
VAR anglais gestion sgbd econometrie sas;
RUN;
```

## **Exercice 2 :**

```
PROC FORMAT;
VALUE $fsexe 
	"F"="FEMININ" 
	"M"="MASCULIN";
VALUE freussite
	1="REUSSITE 1ERE SESSION" 
	2="REUSSITE 2EME SESSION"
	3="REFUS";
RUN;
```

### 1) A partir de la table `resul`, et de différentes `PROC FREQ`, éditez les sorties suivantes : Tableaux de fréquences des variables sexe et reussite

```
PROC FREQ DATA=td7.resul;
TABLE sexe reussite;
FORMAT sexe $fsexe. reussite freussite.;
RUN;
```

### 2) A partir de la table `resul`, et de différentes `PROC FREQ`, éditez les sorties suivantes : Tableaux de fréquences croisées des variables sexe et reussite

```
PROC FREQ DATA=td7.resul;
TABLE sexe*reussite;
FORMAT sexe $fsexe. reussite freussite.;
RUN;
```

### 3) A partir de la table `resul`, et de différentes `PROC FREQ`, éditez les sorties suivantes : Tableaux de fréquences croisées des variables sexe et reussite

```
PROC FREQ DATA=td7.resul;
TABLE sexe*reussite / CROSSLIST NOCOL NOROW NOPERCENT;
FORMAT sexe $fsexe. reussite freussite.;
RUN;
```

### 4) A partir de la table `resul`, et de différentes `PROC FREQ`, éditez les sorties suivantes : Tableau de fréquences de la variable reussite en fonction du sexe 

```
PROC SORT DATA=td7.resul;
BY sexe;
RUN;

PROC FREQ DATA=td7.resul;
BY sexe;
TABLE reussite;
FORMAT sexe $fsexe. reussite freussite.;
RUN;
```

### 5) A partir de la table `resul`, et de différentes `PROC FREQ`, éditez les sorties suivantes : Le graphique de fréquences cumulées de la variable reussite

```
ODS GRAPHICS ON;

PROC FREQ DATA=td7.resul;
TABLE reussite / PLOTS=CUMFREQPLOT;
FORMAT reussite freussite.;
RUN;
```

## **Exercice 3 :**

### 1) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : Calculez le minimum, la moyenne et le maximum pour les matières Anglais, Gestion, SGBD et SAS :

```
PROC TABULATE DATA=td7.siad;
VAR anglais gestion sgbd econometrie sas;
TABLE (anglais gestion sgbd sas),min='Minimum' mean='Moyenne' max='Maximum' / RTS=15;
RUN;
```

### 2) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : Refaire le tableau précédent en faisant un tableau par sexe 

```
PROC FORMAT;
VALUE $fsexe 
	"F"="FEMININ" 
	"M"="MASCULIN";
RUN;

PROC TABULATE DATA=td7.siad;
CLASS sexe;
VAR anglais gestion sgbd econometrie sas;
FORMAT sexe $fsexe.;
TABLE sexe = '', (anglais gestion sgbd sas),min='Minimum' mean='Moyenne' max='Maximum' / RTS=15;
RUN;
```

### 3) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : Calculez la moyenne d’Anglais et de Gestion par sexe et par âge 

```
PROC TABULATE DATA=td7.siad;
VAR anglais gestion;
CLASS sexe age;
FORMAT sexe $fsexe.;
TABLE age, sexe=''*(anglais='ANGLAIS' gestion)*mean=''*F=10.1 / RTS=15 BOX="Moyenne par sexe et age";
RUN;
```

### 4) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : Calculez la moyenne de SGBD, Econométrie et SAS par sexe et par âge 

```
PROC TABULATE DATA=td7.siad;
var sgbd econometrie sas;
CLASS sexe age;
FORMAT sexe $fsexe.;
TABLE age, (sgbd econometrie sas)*sexe=''*mean='Moyenne'*F=9. / RTS=5;
RUN;
```

### 5) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : La Moyenne et l’Ecart-Type de SGBD, Econométrie et SAS en fonction de l’âge et du sexe 

```
PROC FORMAT;
VALUE fnotes  
0 -< 10='Moins de 10/20'
10 -< 15='Entre 10 et 15/20'
15-20='Plus de 15/20';

VALUE fage
low - 20='20 ans et moins'
21 - high='21 ans et plus';
RUN;

PROC TABULATE DATA=td7.siad;
VAR sgbd econometrie sas;
CLASS sexe age;
FORMAT age fage.;
TABLE age*(sexe ALL='Total') ALL='Total',(sgbd econometrie sas)*(mean='Moyenne' std='Ecart Type') * F=8.2 / RTS=22;
RUN;
```

### 6) A partir de la table `siad`, et de différentes `PROC TABULATE`, éditez les sorties suivantes : Le nombre d’élèves et le pourcentage d’élèves par tranche de note en SGBD et Econométrie par sexe et tranche d’âge 

```
PROC TABULATE DATA=td7.siad;
CLASS sexe age sgbd econometrie;
FORMAT sgbd econometrie fnotes. age fage.;
TABLE (sgbd econometrie),(sexe*(age*(N*F=3. ROWPCTN='%'*F=3.)) ALL='Total sexe'*N*F=7.)/RTS=30;
KEYLABEL N="Nombre d'élèves";
RUN;
```
















# TD 8

## **Exercice 1 :**

### **Question 1 :** Affichez par sexe, le prénom et la note d’économétrie. Cette dernière étant triée dans l’ordre décroissant :
```
LIBNAME td8 'C:\Users\16990\Downloads\TD8';
PROC SORT DATA=td8.siad;
BY descending econometrie;
RUN;

PROC REPORT DATA=td8.siad NOWD;
COLUMN sexe prenom econometrie; 
DEFINE sexe / ORDER;
DEFINE econometrie / "Note d'Econométrie";
RUN;
```

### **Question 2 :** Affichez par sexe et par âge la note moyenne, minimum et maximum obtenue en SAS par les étudiants :
```
PROC REPORT DATA=td8.siad NOWD;
COLUMN sexe age sas sas=sas2 sas=sas3;
DEFINE sexe / GROUP;
DEFINE age / GROUP CENTER;
DEFINE sas / MEAN RIGHT FORMAT=4.1 "Note Moyenne SAS";
DEFINE sas2 / MIN RIGHT "Note Minimum SAS";
DEFINE sas3 / MAX RIGHT "Note Maximum SAS";
RUN;
```

### **Question 3 :** Calculez la note moyenne obtenue en SAS par les étudiants en croisant le sexe par l’âge :
```
PROC REPORT DATA=td8.siad NOWD;
COLUMN sexe age, sas;
DEFINE sexe / GROUP;
DEFINE age / ACROSS CENTER;
DEFINE sas / MEAN RIGHT FORMAT=4.1;
RUN;
```


## **Exercice 2 :**

### Pour l’ensemble des questions portant sur la construction de graphiques vous penserez à nommer vos axes, votre légende, à donner un titre à votre graphique et des couleurs à vos graphes si cela est nécessaire.

### **Question 1 :** Importez, à l’aide d’une étape `DATA`, le fichier `bourse.csv`.
```
FILENAME td8 'C:\Users\16990\Downloads\TD8';

DATA bourse;
INFILE td8 (bourse.csv) DLM=';' FIRSTOBS=2;
FORMAT date DATE9.;
INPUT date cours_cac cours_airfrance rendement_cac rendement_airfrance;
RUN;
```

### **Question 2 :** Présentez sous la forme de barres verticales, la somme des rentabilités mensuelles pour le CAC 40 et le titre Air France.
```
TITLE "Rentabilités Mensuelles du CAC 40 et de l'action Air France";
PROC SGPLOT DATA=bourse;
VBAR date / RESPONSE=rendement_cac STAT=SUM TRANSPARENCY=0.4 LEGENDLABEL='CAC 40';
VBAR date / RESPONSE=rendement_airfrance TRANSPARENCY=0.6 LEGENDLABEL='Air France';
YAXIS LABEL='Rentabilité Mensuelle';
FORMAT date FRADFWDX3.;
RUN;
```

### **Question 3 :** Présentez sous la forme d’une ligne, l’évolution moyenne du cours du CAC 40.
```
TITLE "Evolution Moyenne du CAC 40";
PROC SGPLOT DATA=bourse;
VLINE date / RESPONSE=cours_cac STAT=MEAN;
YAXIS LABEL='Cours du CAC 40';
FORMAT date FRADFWDX3.;
RUN;
```

### **Question 4 :** Présentez sous la forme de box plot, les rendements du CAC 40 par mois.
```
TITLE "Rentabilités Mensuelles du CAC 40";
PROC SGPLOT DATA=bourse;
XAXIS OFFSETMIN=0.1 OFFSETMAX=0.1;
VBOX rendement_cac / CATEGORY=date;
FORMAT date FRADFWDX3.;
RUN;
```

### **Question 5 :** Créez les variables `rendement_cac1` et `rendement_cac2` qui correspondent respectivement à la valeur du rendement du CAC 40 avant le 15 Septembre 2008 et la valeur du rendement du CAC 40 après le 15 Septembre 2008.

###  Présentez sous la forme d’histogrammes et de courbes de densité  les deux variables que vous venez de créer. Les courbes de densité seront de type `NORMAL` et la légende sera affichée en haut à gauche du graphique.

```
DATA bourse;
SET bourse;
IF date<='15SEP2008'd THEN rendement_cac1=rendement_cac;
ELSE rendement_cac2=rendement_cac;
RUN;

TITLE "Histogramme et densité du rendement du CAC 40";
PROC SGPLOT DATA=bourse;
HISTOGRAM rendement_cac2 / SCALE=COUNT TRANSPARENCY=0.8 LEGENDLABEL='Après le 15 Septembre 2008' NAME='a';
HISTOGRAM rendement_cac1 / TRANSPARENCY=0.4 LEGENDLABEL='Avant le 15 Septembre 2008' NAME='b';
DENSITY rendement_cac2 / TYPE=NORMAL;
DENSITY rendement_cac1 / TYPE=NORMAL;
KEYLEGEND 'b' 'a' / LOCATION=INSIDE POSITION=TOPLEFT;
RUN;
```

### **Question 6 :** Présentez sous la forme d’une courbe l’évolution du CAC 40 et sous la forme d’un nuage de point l’évolution du cours de l’action Air France.
```
TITLE "Evolution du CAC 40 et de l'action Air France";
PROC SGPLOT DATA=bourse;
SERIES X=date Y=cours_cac / CURVELABEL='CAC 40' CURVELABELLOC=OUTSIDE LINEATTRS=(COLOR=red);
SCATTER X=date Y=cours_airfrance / Y2AXIS MARKERATTRS=(SYMBOL=starfilled COLOR=blue);
FORMAT date DATE7.;
RUN;
```

### **Question 7 :** Présentez sous la forme d’une régression linéaire, le rendement du CAC 40 en fonction du rendement du titre Air France.
```
TITLE "Rendement du CAC 40 et de l'action Air France";
PROC SGPLOT DATA=bourse;
REG X=rendement_cac Y=rendement_airfrance / CLI='IC prévisions individuelles' CLM='IC prévisions moyennes';
LABEL rendement_cac="Rendement du CAC 40" rendement_airfrance="Rendement de l'action Air France";
RUN;
```
















# TD 9

## **Exercice 1 :**

### **Question 1 :** Créez, à l’aide de la `PROC FREQ`, un fichier `frequence.rtf` avec à l’intérieur le calcul de la fréquence de salariés par direction et par ville. Il y aura un tableau par ville.

### Concernant la mise en forme :
### -	Vous afficherez chacun des tableaux dans des pages différentes et le style de vos tableaux sera `BARRETTSBLUE`.

```
LIBNAME td9 'C:\Documents and Settings\Matthieu\Bureau\Cours de SAS\Préparation\TD';

PROC SORT DATA=td9.salaire OUT=salaire;
BY ville;
RUN;

OPTIONS ORIENTATION=PORTRAIT NONUMBER CENTER NODATE;
ODS RTF FILE='affichage.rtf' STYLE=sasweb STARTPAGE=YES;
TITLE 'Nombre de salariés par direction et par ville';

PROC FREQ DATA=salaire;
BY ville;
TABLES direction;
RUN;

ODS RTF CLOSE;
```

### **Question 2 :** Refaire la question précédente en n’affichant que les graphiques des fréquences.

```
OPTIONS ORIENTATION=PORTRAIT NONUMBER CENTER NODATE;
ODS GRAPHICS ON;
ODS RTF FILE='affichage.rtf' STYLE=sasweb STARTPAGE=YES;
TITLE 'Nombre de salariés par direction et par ville';

PROC FREQ DATA=td9.salaire;
BY ville;
TABLES direction / plots = all;
RUN;

ODS RTF CLOSE;
ODS GRAPHICS OFF;
ODS SELECT ALL;
```

### **Question 3 :** Affichez, à l’aide de la `PROC PRINT`, dans une **sortie HTML**, les variables `Direction` et `Salaire` des salariés de la ville de Toulouse. Vous ajouterez la somme des salaires de ces employés.

### Concernant la mise en forme :
### -	Toutes vos données (en-tête, données, somme) seront écrites en Arial avec une taille de 3
### -	Vous donnerez une couleur de fond et une couleur de police différente pour chacune de vos données (en-tête, données, somme)
### -	Les données de la ligne somme seront en gras et vous ajouterez un texte avant et celle-ci pour la présenter (Somme des salaires : X €)

```
FILENAME odsout 'C:\Documents and Settings\Mes documents';

ODS HTML PATH=odsout BODY='corps.html';

PROC PRINT DATA=salaire NOOBS
STYLE(HEADER)={JUST=center BACKGROUND=blue FOREGROUND=red FONT_SIZE=3 FONT_FACE='Arial'}
STYLE(DATA)={JUST=center BACKGROUND=yellow FOREGROUND=green FONT_SIZE=3 FONT_FACE='Arial'}
STYLE(GRANDTOTAL)={JUST=center BACKGROUND=red FOREGROUND=blue FONT_WEIGHT=bold FONT_SIZE=3 FONT_FACE='Arial'};
VAR identifiant direction salaire;
SUM salaire / STYLE(GRANDTOTAL)={PRETEXT='Somme des salaires : ' POSTEXT=' €'};
WHERE ville='Toulouse';
RUN;

ODS HTML CLOSE;
```

### **Question 4 :** Question 4 : Calculez l’ancienneté de chacun des salariés en années.

```
DATA salaire;
SET salaire;
anciennete=INTCK("YEAR",embauche,today());
RUN;
```

### **Question 5 :** Créer, à l’aide de la PROC TABULATE, un tableau calculant par `Ville` et par `Direction` la somme des salaires, le salaire moyen et l’ancienneté moyenne.

```
ODS HTML PATH=odsout BODY='corps.html' STYLE=barrettsblue;
PROC TABULATE DATA=salaire;
CLASS ville direction;
VAR salaire anciennete;
TABLE ville=''*direction='', salaire=''*(SUM='Salaire Total' MEAN='Salaire Moyen')*F=10. 
							 anciennete=''*MEAN='Ancienneté Moyenne' / BOX='Villes / Directions';
RUN;
ODS HTML CLOSE;
```

### **Question 6 :** Créez, à l’aide des procédures des Question 1, 3 et 5, une **sortie HTML** contenant une table de matières.

### Concernant la mise en forme :
### -	Tous les tableaux seront dans le style « BARRETTSBLUE »
### -	Vous modifierez la table de matières afin que celle-ci soit plus lisible.
### -	Vous ajouterez un titre **dans la sortie** pour chacune des procédures.
### -	Vous n’afficherez pas le nom des procédures **dans la sortie** mais vous mettrez un titre plus parlant.
### Vous donnerez un titre à votre page.

```
ODS HTML PATH=odsout BODY='corps.html' CONTENTS='tdm.html' FRAME='resultat.html' (TITLE='Informations Salariés') STYLE=barrettsblue;
ODS NOPROCTITLE;

ODS PROCLABEL "Nombre de salariés par direction";
TITLE "Nombre de salariés par direction";
PROC FREQ DATA=salaire;
BY ville;
TABLES direction;
RUN;

ODS PROCLABEL "Informations sur les salariés de Toulouse";
TITLE "Informations sur les salariés de Toulouse";
PROC PRINT DATA=salaire NOOBS;
VAR identifiant direction salaire;
SUM salaire / STYLE(GRANDTOTAL)={PRETEXT='Somme des salaires : ' POSTEXT=' €'};
WHERE ville='Toulouse';
RUN;

ODS PROCLABEL "Salaire et Ancienneté par ville et direction";
TITLE "Salaire et Ancienneté par ville et direction";
PROC TABULATE DATA=salaire;
CLASS ville direction;
VAR salaire anciennete;
TABLE ville=''*direction='', salaire=''*(SUM='Salaire Total' MEAN='Salaire Moyen')*F=10. 
							 anciennete=''*MEAN='Ancienneté Moyenne' / BOX='Villes / Directions';
RUN;

ODS HTML CLOSE;
```
















# TD 10

## **Exercice 1 :**

```
LIBNAME td10 'c:\.....';
```

### **Question 1 :** A partir de la table `salaire`, créez une nouvelle table avec les salariés embauchés avant le 1er Janvier 1990 qui ont un salaire supérieur à 42 000 euros par an.

```
DATA q1;
SET td10.salaire;
IF embauche<'01JAN1990'd AND salaire>42000;
RUN;
```

### **Question 2 :** A partir de la table créée en question 1, affichez dans la sortie les variables `ville` et `direction`.

```
PROC PRINT DATA=q1;
VAR ville direction;
RUN;
```

### **Question 3 :** A partir de la table `salaire`, effectuez un tableau de fréquence de la variable `direction` pour les villes de Lille et Toulouse.

```
PROC FREQ DATA=td10.salaire;
TABLES direction;
WHERE ville IN('Lille' 'Toulouse');
RUN;
```

### **Question 4 :** A partir de la table `salaire`, et de la procédure adéquate, déterminez quel est le salaire moyen dans les villes de Lille et Toulouse.

```
PROC MEANS DATA=td10.salaire MEAN;
VAR salaire;
WHERE ville IN('Lille' 'Toulouse');
RUN;
```

### **Question 5 :** L’objectif de cette question est de refaire les quatre questions précédentes au sein d’une même **macro** que vous appellerez `TD10`. Cette macro aura les paramètres suivants :

-	`tab` : qui va nous permettre de choisir la table que nous allons utiliser,

-	`emb` : qui va vous permettre de faire varier la date d’embauche pour la création de la première table,

-	`sal` : qui va vous permettre de faire varier le seuil du salaire pour la création de la première table,

-	`var1` : qui va vous permettre d’afficher les variables que vous souhaitez voir apparaître dans votre sortie,

-	`var2` : qui va vous permettre de faire le tableau de fréquence sur cette ou ces variables,

-	`ville` : qui va vous permettre de faire le tableau de fréquence et de calculer le salaire moyen sur cette ou ces villes.

```
%MACRO td10(tab=, emb=, sal=, var1=, var2=, ville=);
DATA q1;
SET &tab;
IF embauche<&emb AND salaire>&sal;
RUN;

PROC PRINT DATA=q1;
VAR &var1;
RUN;

PROC FREQ DATA=&tab;
TABLES &var2;
WHERE ville IN(&ville);
RUN;

PROC MEANS DATA=&tab MEAN;
VAR salaire;
WHERE ville IN(&ville);
RUN;
%MEND;
```

### **Question 6 :** Exécuter la macro `TD10` en faisant varier vos paramètres.

```
%td10(tab=td10.salaire, emb='01JAN1990'd, sal=42000, var1=ville direction, var2=direction, ville='Lille' 'Toulouse');

```

















