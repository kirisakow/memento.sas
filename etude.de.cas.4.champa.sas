data champa;
/*
Y : consommation de champagne en volume par personne
X1: revenu par personne
X2: prix du champagne en euros constants
X3: prix des liqueurs et apéritifs en euros constants
*/
set '/home/u43061992/sasuser.v94/PUS_Econometrie-sous-SAS/champa.sas7bdat';
lnY=log(Y);
lnX1=log(X1);
lnX2=log(X2);
lnX3=log(X3);
run;

/* Premier modèle avec les trois variables */
proc reg data=champa;
model lnY=lnX1 lnX2 lnX3;
run;
/*
On obtient :
	⋅ R² = 0.9925	=>	le modèle explique plus de 99% des variations observées.
	⋅ lnY = 0.51721 + 2.37141⋅lnX1 -1.37394⋅lnX2 -0.10799⋅lnX3		<=>		Y = exp(0.51721)⋅X1^(2.37141)⋅X2^(-1.37394)⋅X3^(-0.10799)
	⋅ Modèle multivarié => question : le modèle est-il globalement significatif ? (au moins une var est-elle significative ?)
		H_0 : aucune des variables n'a d'impact sur Y (autrement dit, pour tout j, tous les a_j=0) ;
		H_1 : au moins une des variables a un impact sur Y (autrement dit, il existe au moins un j tel que a_j≠0).
	  Pour cela, test de Fisher : on peut voir dans les résultats Pr>F <.0001 (plausibilité extrêmement faible de H_0)
	  =>	on retient H_1 : au moins une var agit sur la conso de champagne en volume	=>	le modèle est globalement très significatif.
	⋅ Les coefficients a_1, a_2, a_3 sont ce qu'on appelle en économie des « élasticités » :
		⋅ X1 étant le revenu des consommateurs, a_1 est l' « élasticité revenu » de Y (consommation de champagne) ;
			⋅ logiquement, a_1 devrait être sup à 0 voire sup à 1 et il l'est ;
			⋅ test de Student : Pr>|t| <.0001
				=>	plausibilité extrêmement faible pour H_0 ;
				=>	impact hautement significatif de X1 sur Y ;
		⋅ X2 étant le prix du champagne en euros constants, a_2 est l' « élasticité prix directe » de Y ;
			⋅ logiquement, a_2 devrait être inf à 0 et il l'est ;
			⋅ test de Student : Pr>|t| <.0001
				=>	plausibilité extrêmement faible pour H_0 ;
				=>	impact hautement significatif de X2 sur Y ;
		⋅ X3 étant le prix des liq. et apér. en euros constants, a_3 est l' « élasticité prix croisée » de Y ;
			⋅ logiquement, a_2 devrait être inf à 0 mais il ne l'est pas (et c'est surprenant) ;
			⋅ test de Student : Pr>|t| = 0.0927 > 0.05
				=>	plausibilité significative pour H_0 ;
				=>	impact extrêmement faible de X3 sur Y	=>	on devrait éliminer X3 de notre modèle.
*/

/* Test de Fisher partiel */
proc reg data=champa;
model lnY=lnX1 lnX2 lnX3;
Fisher_partiel_X3: test lnX3=0;
run;
/*
On retrouve le même Pr>F = 0.0927 que Pr>|t| trouvé précédemment
(en effet, le Fisher-partiel n'est rien d'autre que le t calculé de Student élevé au carré)
*/

/* Nouveau modèle avec deux variables */
proc reg data=champa;
model lnY=lnX1 lnX2;
run;
/*
On obtient :
	⋅ R² = 0.9906	=>	le modèle explique plus de 99% des variations observées.
	⋅ lnY = 0.10731 + 2.19863⋅lnX1 -1.21913⋅lnX2	<=>		Y = exp(0.10731)⋅X1^(2.19863)⋅X2^(-1.21913)

Ce modèle recalibré est-il satisfaisant ?

	⋅ Test de significativité globale du modèle : test de Fisher : 
		Pr>F <.0001		=>	plausibilité extrêmement faible de H_0 (hypothèse que le modèle n'a aucun impact sur Y)
						=>	on retient donc H_1 (hypothèse qu'au moins une des var a un impact sur Y)
						=>	le modèle est globalement très significatif.
	⋅ Test de significativité de chacune des variables : test de Student :
		⋅ pour X1 : Pr>|t| <.0001	<0.001	=> action hautement significative sur Y 
		⋅ pour X1 : Pr>|t| =0.0002	<0.001	=> action hautement significative sur Y
	=> On peut donc conclure que le modèle est statistiquement satisfaisant.
*/


/*
Tester l'hypothèse « le champagne est un bien de luxe » :
	H_0 : le champagne n'est pas un bien de luxe (augmentation de la conso de cham. est proport. à l'augm. du revenu) :	a_1 = 1
	H_1 : le champagne est un bien de luxe (augm. de la conso de cham. est plus que proport. à l'augm. du revenu) : 	a_1 > 1
	
	C'est un test unilatéral droit.
*/
proc reg data=champa;
model lnY=lnX1 lnX2;
Fisher_partiel_X1: test lnX1=1;
run;
/*
On obtient :
	Pr>F <.0001		=>	plausibilité extrêmement faible de H_0
					=>	on retient donc H_1 
					=>	le champ est donc un bien de luxe.
*/

/*
Tester l'hypothèse « la baisse du prix de cham entraîne, ttes choses égales par ailleurs, une hausse de la conso en volume » :
	H_0 : a_2 = 0
	H_1 : a_2 < 0

	C'est un test unilatéral gauche.
	
	...
*/

/*
Tester l'hypothèse « la baisse du prix de cham entraîne, ttes choses égales par ailleurs, une hausse de la conso EN VALEUR » :
(conso en valeur = volume⋅prix = Y⋅X2 = exp(0.10731)⋅X1^(2.19863)⋅X2^(-1.21913 + 1))
	H_0 : a_2 = -1
	H_1 : a_2 + 1 < 0	<=> a_2 < -1

	C'est un test unilatéral gauche.
*/
proc reg data=champa;
model lnY=lnX1 lnX2;
Fisher_partiel_X2: test lnX2=-1;
run;
/*
On obtient :
	Pr>F = 0.3819 pour test bilatéral	=>	diviser par deux pour notre test unilatéral gauche
										=>	l'indicateur de plausibilité de H_0 est 0.19095 > 0.05
										=>	on ne peut rejeter H_0
										=>	la baisse du prix de cham n'entraîne pas de hausse de la conso en valeur.
*/

/* 
Model-Selection Methods:

	⋅ selection=backward : "At each step, the variable showing the smallest contribution to the model is deleted."
	⋅ selection=forward : "At each step, it adds to the model the variable that has the largest F statistic.
	                       If no F statistic has a significance level greater than the SLENTRY= value (0.15 if the 
	                       SLENTRY= option is omitted), the FORWARD selection stops. Once a variable is in the 
	                       model, it stays."
	⋅ selection=stepwise : "The stepwise method is a modification of the forward-selection technique and differs 
	                        in that variables already in the model do not necessarily stay there. The stepwise process 
	                        goes on until none of the variables outside the model has an F statistic significant at 
	                        the SLENTRY= level and every variable in the model is significant at the SLSTAY= level; 
	                        or when the variable to be added to the model is the one just deleted from it."
*/
proc reg data=champa;
model lnY=lnX1 lnX2 lnX3 / selection=backward
                           slstay=0.05; /* par défaut 0.1 */
run;

/* 
Réalisation d'un stepwise ASCENDANT
(ajouts successifs de var sup ou égales à un seuil de significativité)
*/
proc reg data=champa;
model lnY=lnX1 lnX2 lnX3 / selection=stepwise
                           slstay=0.05
                           slentry=0.05; /* par défaut 0.15 */
run;

